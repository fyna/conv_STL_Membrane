TARGET=geometry.stl
TARGET_MEMBRANE=geometry_membrane.stl

INLET_DRAW_SOURCE=INLET_DRAW_HF060317.stl
OUTLET_DRAW_SOURCE=OUTLET_DRAW_HF060317.stl
WALL_DRAW_SOURCE=WALL_DRAW_HF060317.stl

INLET_FEED_SOURCE=INLET_FEED_HF060317.stl
OUTLET_FEED_SOURCE=OUTLET_FEED_HF060317.stl
# WALL_FEED_SOURCE=comsolSTL_WallFeed.stl

MEMBRANE_SOURCE=MEMBRANE_HF060317.stl

#	delete file if existent and create new, empty file
[ -f $TARGET	 ] && rm $TARGET
touch $TARGET

#	same for membrane file, which must be separate from the outer geometry
[ -f $TARGET_MEMBRANE	 ] && rm $TARGET_MEMBRANE
touch $TARGET_MEMBRANE

sed -i '1 s/^.*$/solid inletDraw/' $INLET_DRAW_SOURCE
cat $INLET_DRAW_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid outletDraw/' $OUTLET_DRAW_SOURCE
cat $OUTLET_DRAW_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid wallDraw/' $WALL_DRAW_SOURCE
cat $WALL_DRAW_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid inletFeed/' $INLET_FEED_SOURCE
cat $INLET_FEED_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid outletFeed/' $OUTLET_FEED_SOURCE
cat $OUTLET_FEED_SOURCE >> $TARGET

# sed -i '1 s/^.*$/solid wallFeed/' $WALL_FEED_SOURCE
# cat $WALL_FEED_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid membrane/' $MEMBRANE_SOURCE
cat $MEMBRANE_SOURCE >> $TARGET_MEMBRANE
